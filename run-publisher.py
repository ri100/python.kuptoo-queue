import json

from kombu import Connection

from kuptoo_queue.publisher.queue_publisher import QueuePublisher
from kuptoo_queue.worker.queue_meta import QueueMeta


file = '/mnt/data/corpus_annotated/allegro/dataset-20/test.json'


with Connection('amqp://guest:guest@127.0.0.1:5672//', connect_timeout=120) as conn:

    queue = QueuePublisher(conn, out_queue=QueueMeta('kuptoo.insert', routing_key='offer.NEW'))

    i, batch = 0, 500
    skip = 0
    payload = []
    for line in open(file):
        data = json.loads(line)
        i += 1

        if i < skip:
            continue

        title = data['title']['std']

        load = {
            "offer": {
                'title': {
                    'raw': title,
                },
            }
        }

        payload.append(load)
        if i % batch == 0:
            queue.publish(payload)
            # print(payload)
            payload = []

