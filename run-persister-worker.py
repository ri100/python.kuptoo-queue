from kombu import Connection

from kuptoo_queue.hander.dummy import DummyHandler
from kuptoo_queue.worker.queue2handler import Queue2Handler
from kuptoo_queue.worker.queue_meta import QueueMeta

while True:
    with Connection('amqp://guest:guest@127.0.0.1:5672//', connect_timeout=20) as conn:
        handler = DummyHandler()
        in_queue = QueueMeta('kuptoo.persister', routing_key='offer.TAG')
        worker = Queue2Handler(handler, conn, in_queue, worker_name_prefix='persister-')

        worker.run()
