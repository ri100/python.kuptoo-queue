from kombu import Connection
from kuptoo_queue.hander.dummy import DummyHandler
from kuptoo_queue.worker.queue2queue import Queue2QueueWorker
from kuptoo_queue.worker.queue_meta import QueueMeta

with Connection('amqp://guest:guest@127.0.0.1:5672//', connect_timeout=20) as conn:
    handler = DummyHandler()
    in_queue = QueueMeta('kuptoo.insert', routing_key='offer.NEW')
    out_queue = QueueMeta('kuptoo.persister', routing_key='offer.TAG')
    worker = Queue2QueueWorker(handler,
                               conn,
                               in_queue,
                               out_queue,
                               worker_name_prefix='annotator-')
    worker.run()
