from setuptools import setup

setup(
    name='kuptoo_queue',
    version='0.6.1',
    description='Facade for queue handling',
    author='Risto Kowaczewski',
    author_email='risto.kowaczewski@gmail.com',
    packages=['kuptoo_queue'],
    install_requires=['kombu==4.2.1'],
    include_package_data=True
)
