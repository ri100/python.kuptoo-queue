class QueueMeta:
    def __init__(self, name, routing_key, queue_type='direct', durable=True):
        self.durable = durable
        self.queue_type = queue_type
        self.routing_key = routing_key
        self.name = name