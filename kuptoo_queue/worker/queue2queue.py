import logging

from kombu import Exchange, Queue
from kombu.mixins import ConsumerMixin
from kuptoo_queue.worker.queue_meta import QueueMeta


logging.basicConfig(format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)


class Queue2QueueWorker(ConsumerMixin):

    def __init__(self, payload_handler, connection, in_queue: QueueMeta, out_queue: QueueMeta, worker_name_prefix=None):
        self._worker_name_prefix = worker_name_prefix
        self._payload_handler = payload_handler
        self.out_queue = out_queue
        self.exchange_insert = Exchange(in_queue.name, in_queue.queue_type, durable=in_queue.durable)
        self.queue_insert = Queue(in_queue.name, exchange=self.exchange_insert, routing_key=in_queue.routing_key)
        self.connection = connection
        self.exchange_tags = Exchange(out_queue.name, out_queue.queue_type, durable=out_queue.durable)
        self.queue_tags = Queue(out_queue.name, exchange=self.exchange_tags, routing_key=out_queue.routing_key)

    def get_consumers(self, consumer, channel):
        return [
            consumer(queues=[self.queue_insert], callbacks=[self.handle_payload],
                     auto_declare=True, prefetch_count=20,
                     tag_prefix=self._worker_name_prefix),
        ]

    def on_connection_error(self, exc, interval):
        logger.error("RabbitMq {}. Reconnecting in  {} seconds".format(exc, interval))

    def on_connection_revived(self):
        logger.error("RabbitMq connected")

    def handle_payload(self, payload_in, message):
        payload_out = self._payload_handler.handle(payload_in)

        # Queue
        producer = self.connection.Producer(serializer='json')
        producer.publish(payload_out,
                         exchange=self.exchange_tags, routing_key=self.out_queue.routing_key,
                         declare=[self.queue_tags])  # , compression='bzip2'

        message.ack()
