import logging
from time import sleep

from kombu import Exchange, Queue
from kombu.mixins import ConsumerMixin
from kuptoo_queue.worker.queue_meta import QueueMeta


logging.basicConfig(format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)


class Queue2Handler(ConsumerMixin):

    def __init__(self, payload_handler, connection, in_queue: QueueMeta, worker_name_prefix=None,):
        self._payload_handler = payload_handler
        self.worker_name_prefix = worker_name_prefix
        exchange = Exchange(in_queue.name, in_queue.queue_type, durable=in_queue.durable)
        self.queue = Queue(in_queue.name, exchange=exchange, routing_key=in_queue.routing_key)
        self.connection = connection

    def get_consumers(self, Consumer, channel):
        return [
            # There may be several consumers
            Consumer(queues=[self.queue], callbacks=[self.handle_payload],
                     auto_declare=True, prefetch_count=20,
                     tag_prefix=self.worker_name_prefix),
        ]

    def on_connection_error(self, exc, interval):
        logger.error("RabbitMq {}. Reconnecting in  {} seconds".format(exc, interval))

    def on_connection_revived(self):
        logger.error("RabbitMq connected")

    def handle_payload(self, payload_in, message):
        self._payload_handler.handle(payload_in)
        message.ack()
