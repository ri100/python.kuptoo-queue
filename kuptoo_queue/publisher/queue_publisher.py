import logging

from kombu import Exchange, Queue, Connection

from kuptoo_queue.worker.queue_meta import QueueMeta

logging.basicConfig(format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class QueuePublisher:

    def __init__(self, conn, out_queue: QueueMeta):
        self.conn = conn
        self.out_queue = out_queue
        self.exchange = Exchange(out_queue.name, out_queue.queue_type, durable=out_queue.durable)
        self.queue = Queue(out_queue.name, exchange=self.exchange, routing_key=out_queue.routing_key)
        producer = self.conn.Producer(serializer='json', auto_declare=True)
        self.publisher = self.conn.ensure(producer, producer.publish, errback=self.error_reporter, max_retries=3)

    @staticmethod
    def error_reporter(exc, interval):
        logger.info('Error: {}', str(exc))
        logger.info('Retry in %s seconds.', interval)

    def publish(self, payload):
        self.publisher(payload,
                       retry=True,
                       exchange=self.exchange, routing_key=self.out_queue.routing_key,
                       declare=[self.queue])  # , compression='bzip2'
